<?php
   include "../user/session2.php";
   include "../../koneksi.php";
   if(isset($_GET['nip'])){
       $nip = $_GET['nip'];
       $select = mysqli_query($host, "SELECT * FROM guru WHERE nip='$nip'") or die(mysqli_error($host));
       if(mysqli_num_rows($select) == 0){
           echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
           exit();
       }else{
           $d = mysqli_fetch_assoc($select);
       }
   }
   if(isset($_POST['submit'])){
       $nama_guru  = $_POST['nama_guru'];
       $jk         = $_POST['jk'];
       $hp         = $_POST['hp'];
   
       $sql = mysqli_query($host, "UPDATE guru SET nama_guru='$nama_guru', jk='$jk', hp='$hp' WHERE nip='$nip'") or die(mysqli_error($host));
   
       if($sql){
           $_SESSION["sukses"] = 'Data guru berhasil diedit!';
       }else{
           $_SESSION["gagal"] = 'Data guru gagal diedit!';
           }
   }
   ?>
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Edit</a></li>
               <li class="breadcrumb-item active">Guru</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-primary">
                  <h3 class="card-title">Edit Data Guru</h3>
               </div>
               <div class="card-body">
                  <form method="post" action="../index/?page=edit_guru&nip=<?php echo $nip;?>">
                     <table class="table">
                        <tr>
                           <td>NIP</td>
                           <td><input type="text" name="nip" value="<?php echo $d['nip'];?>" readonly disabled></td>
                        </tr>
                        <tr>
                           <td>Nama Guru</td>
                           <td><input type="text" name="nama_guru" value="<?php echo $d['nama_guru'];?>" required></td>
                        </tr>
                        <tr>
                           <td>Jenis Kelamin</td>
                           <td>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="jk" value="L" required>
                                 <label class="form-check-label">Laki-laki</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="jk" value="P" required>
                                 <label class="form-check-label">Perempuan</label>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>Nomor HP</td>
                           <td><input type="text" name="hp" value="<?php echo $d['hp'];?>" required></td>
                        </tr>
                     </table>
                     <a href="../index/?page=guru" class="btn btn-secondary">Kembali</a>
                     <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>