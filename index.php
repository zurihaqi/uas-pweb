<?php 
   include "pages/user/session.php";
   include "koneksi.php";
   $activePage = basename($_GET['page'], ".php");
   $name=$_SESSION['login'];
   $selectdata=mysqli_query($host,"SELECT * FROM users WHERE username='$name'");
   $ud=mysqli_fetch_row($selectdata);
   ?>
<html>
   <head>
      <?php include "head.php"; ?>
   </head>
   <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed dark-mode">
   <div class="preloader flex-column justify-content-center align-items-center bg-dark">
      <img class="animation__wobble" src="../plugins/dist/img/school.png" alt="LogoSekolah" height="60" width="60">
   </div>
      <div class="wrapper">
         <nav class="main-header navbar navbar-expand navbar-dark text-sm">
            <?php include "header.php"; ?>
         </nav>
         <aside class="main-sidebar main-sidebar-custom sidebar-dark-primary elevation-4">
            <?php include "sidebar.php"; ?>
         </aside>
         <div class="content-wrapper">
            <?php include "loadpage.php"; ?>
        </div>
         <footer class="main-footer bg-dark text-sm">
            <?php include "footer.php"; ?>
         </footer>
      </div>
      <script src="../plugins/tableconfig.js"></script>
      <script src="../plugins/index.js"></script>
      <?php if (@$_SESSION['sukses']) { ?>
         <script>
               Swal.fire("Sukses!", "<?php echo $_SESSION['sukses']; ?>", 'success');
         </script>
      <?php unset($_SESSION['sukses']);}?>

      <?php if (@$_SESSION['gagal']) { ?>
         <script>
               Swal.fire("Galat!", "<?php echo $_SESSION['gagal']; ?>", 'error');
         </script>
      <?php unset($_SESSION['gagal']);}?>

      <?php if (@$_SESSION['dupe']) { ?>
         <script>
               Swal.fire("Perhatian!", "<?php echo $_SESSION['dupe']; ?>", 'warning');
         </script>
      <?php unset($_SESSION['dupe']);}?>
   </body>
</html>