<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
                Dashboard
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Home</a></li>
               <li class="breadcrumb-item active">Halaman Utama</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>
                    <?php 
                        $result = mysqli_query($host, "SELECT * FROM siswa");
                        $rows = mysqli_num_rows($result);
                        echo "$rows";
                    ?>
                </h3>

                <p>Data Siswa</p>
              </div>
              <div class="icon">
                <i class="icon ion-md-school"></i>
              </div>
              <a href="../index/?page=siswa" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>
                    <?php 
                    $result = mysqli_query($host, "SELECT * FROM guru");
                    $rows = mysqli_num_rows($result);
                    echo "$rows";
                    ?>    
                </h3>

                <p>Data Guru</p>
              </div>
              <div class="icon">
                <i class="icon ion-md-briefcase"></i>
              </div>
              <a href="../index/?page=guru" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>
                    <?php 
                    $result = mysqli_query($host, "SELECT * FROM mapel");
                    $rows = mysqli_num_rows($result);
                    echo "$rows";
                    ?>      
                </h3>

                <p>Data Mapel</p>
              </div>
              <div class="icon">
                <i class="icon ion-md-book"></i>
              </div>
              <a href="../index/?page=mapel" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>
                    <?php 
                    $result = mysqli_query($host, "SELECT * FROM nilai");
                    $rows = mysqli_num_rows($result);
                    echo "$rows";
                    ?>  
                </h3>

                <p>Data Nilai</p>
              </div>
              <div class="icon">
                <i class="icon ion-md-bookmarks"></i>
              </div>
              <a href="../index/?page=nilai" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
   </div>
   <div class="row">
   <div class="col-md-6">
    <div class="card card-dark">
      <div class="card-header">
        
        <h3 class="card-title">
           <i class="fas fa-chart-pie"></i>
            Grafik
        </h3>
    
        <div class="card-tools">
          <button type="button" class="btn btn-light btn-sm" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body bg-light">
        <canvas id="myChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
      </div>
    </div>
    </div>
    <div class="col-md-6">
        <div class="card bg-gradient-light card-dark">
      <div class="card-header border-0">
        <h3 class="card-title">
          <i class="far fa-calendar-alt"></i>
          Kalender
        </h3>
        <div class="card-tools">
          <button type="button" class="btn btn-light btn-sm" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body pt-0">
        <div id="kalender" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></div>
      </div>
    </div>
    </div>
    </div>
</section>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Siswa', 'Guru', 'Mapel', 'Nilai'],
        datasets: [{
        data: [
            <?php 
            $result = mysqli_query($host, "SELECT * FROM siswa");
            $rows = mysqli_num_rows($result);
            echo "$rows";
            ?>,
            <?php 
            $result = mysqli_query($host, "SELECT * FROM guru");
            $rows = mysqli_num_rows($result);
            echo "$rows";
            ?>,
            <?php 
            $result = mysqli_query($host, "SELECT * FROM mapel");
            $rows = mysqli_num_rows($result);
            echo "$rows";
            ?>,
            <?php 
            $result = mysqli_query($host, "SELECT * FROM nilai");
            $rows = mysqli_num_rows($result);
            echo "$rows";
            ?>
        ],
        backgroundColor: [
            'rgba(54, 162, 235, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 99, 132, 1)'
        ],
        hoverOffset: 4
      }]
    },
    options: {
        maintainAspectRatio : false,
        responsive : true,
    }
});
</script>
<script type="text/javascript">
    $(function () {
        $('#kalender').datetimepicker({
            inline: true,
            format: 'L'
        });
    });
</script>