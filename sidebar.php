<?php
   if(!isset($_SESSION["login"])){
    echo"
    <link rel='icon' href='icon.ico' type='../image/ico' />
    <script>alert('Anda belum melakukan login!')
    window.location.href = '../login/';</script>";
};
?>

<a href="../../index/?page=home" class="brand-link nounderline">
<img src="../plugins/dist/img/school.png" alt="School Logo" class="brand-image elevation-3" style="opacity: .8">
<span class="brand-text font-weight-light d-flex">CRUD Sekolah</span>
</a>
<div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
            <a class="d-block">Selamat datang,<br><span style="color: yellow;"><?php printf($ud[1]);?></span></a>
        </div>
    </div>
    <div class="mt-2 form-inline">
    </div>
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
            <a href="../../index/?page=home" class="nav-link <?= ($activePage == 'home') ? 'active':''; ?>">
                <i class="nav-icon fas fa-home"></i>
                <p>
                    Halaman Utama
                </p>
            </a>
            <li class="nav-item">
            <a href="../pages/user/logout.php" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Keluar</p>
            </a>
            </li>
            <li class="nav-header">DATABASE</li>
            </li>
            <li class="nav-item 
            <?php 
            if($activePage == 'siswa'){
                echo "menu-open";
            }else if($activePage == 'guru'){
                echo "menu-open";
            }else if($activePage == 'nilai'){
                echo "menu-open";
            }else if($activePage == 'mapel'){
                echo "menu-open";
            }else {
                echo"";
            }
            ?>">
            <a href="#" class="nav-link 
            <?php 
                if($activePage == 'siswa'){
                    echo "active";
                }else if($activePage == 'guru'){
                    echo "active";
                }else if($activePage == 'nilai'){
                    echo "active";
                }else if($activePage == 'mapel'){
                    echo "active";
                }else {
                    echo"";
                }
            ?>">
                <i class="nav-icon fas fa-database"></i>
                <p>
                    Sekolah
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="../index/?page=siswa" class="nav-link <?= ($activePage == 'siswa') ? 'active':''; ?>">
                        <i class="fas fa-table nav-icon"></i>
                        <p>Tabel Siswa</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="../index/?page=guru" class="nav-link <?= ($activePage == 'guru') ? 'active':''; ?>">
                        <i class="fas fa-table nav-icon"></i>
                        <p>Tabel Guru</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="../index/?page=nilai" class="nav-link <?= ($activePage == 'nilai') ? 'active':''; ?>">
                        <i class="fas fa-table nav-icon"></i>
                        <p>Tabel Nilai</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="../index/?page=mapel" class="nav-link <?= ($activePage == 'mapel') ? 'active':''; ?>">
                        <i class="fas fa-table nav-icon"></i>
                        <p>Tabel Mata Pelajaran</p>
                    </a>
                </li>
            </ul>
            </li>
            <li class="nav-item disabled">
            <a href="../index/?page=audit" class="nav-link <?= ($activePage == 'audit') ? 'active':''; ?> disabled">
                <i class="nav-icon fas fa-book"></i>
                <p>Catatan Audit<br>(dalam proses)</p>
            </a>
            </li>
            <li class="nav-header">PENGATURAN</li>
            <li class="nav-item">
            <a href="../index/?page=setting" class="nav-link <?= ($activePage == 'setting') ? 'active':''; ?>">
                <i class="nav-icon fas fa-id-card"></i>
                <p>Ubah Data Akun</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="../index/?page=profilepic" class="nav-link <?= ($activePage == 'profilepic') ? 'active':''; ?>">
                <i class="nav-icon fas fa-user-circle"></i>
                <p>Ubah Foto Profil</p>
            </a>
            </li>
            <?php 
            //     if($ud[5] == 2)
            //     {
            //     ?>
            <!--<li class="nav-item">-->
            <!--    <a href="../index/?page=srccode" class="nav-link <?=($activePage == 'srccode') ? 'active':'';?>">-->
            <!--        <i class="nav-icon fas fa-download"></i>-->
            <!--        <p>Unduh Source Code</p>-->
            <!--    </a>-->
            <!--</li>-->
            <?php        
            //     }; 
            //     ?>
        </ul>
    </nav>
</div>
<div class="sidebar-custom">
</div>