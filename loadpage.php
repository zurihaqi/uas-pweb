<?php
    $queries = array();
    parse_str($_SERVER['QUERY_STRING'], $queries);
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    switch ($queries['page']) {
    case 'siswa':
        include 'pages/siswa/dash_siswa.php';
        break;
    case 'edit_siswa':
        include 'pages/siswa/edit_siswa.php';
        break;
    case 'guru':
        include 'pages/guru/dash_guru.php';
        break;
    case 'edit_guru':
        include 'pages/guru/edit_guru.php';
        break;
    case 'mapel':
        include 'pages/mapel/dash_mapel.php';
        break;
    case 'edit_mapel':
        include 'pages/mapel/edit_mapel.php';
        break;
    case 'nilai':
        include 'pages/nilai/dash_nilai.php';
        break;
    case 'edit_nilai':
        include 'pages/nilai/edit_nilai.php';
        break;
    case 'setting':
        include 'pages/user/user_setting.php';
        break;
    case 'profilepic':
        include 'pages/user/profilepic.php';
        break;
    case 'srccode':
        include 'srccode.php';
        break;
    case 'home':
        include 'pages/home.php';
        break;
    }
?>