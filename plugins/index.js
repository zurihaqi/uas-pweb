var timestamp = '<?=time();?>';
function updateTime(){
$('#time').html(Date(timestamp));
timestamp++;
}
$(function(){
setInterval(updateTime, 1000);
});

document.addEventListener("DOMContentLoaded", function() {
var elements = document.getElementsByTagName("INPUT");
for (var i = 0; i < elements.length; i++) {
   elements[i].oninvalid = function(e) {
         e.target.setCustomValidity("");
         if (!e.target.validity.valid) {
            e.target.setCustomValidity("Kolom ini tidak boleh kosong.");
         }
   };
   elements[i].oninput = function(e) {
         e.target.setCustomValidity("");
   };
}
});