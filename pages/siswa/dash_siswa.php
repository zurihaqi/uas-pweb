<?php 
   include "../user/session2.php";
   include "../../koneksi.php";
   error_reporting(E_ALL ^ E_WARNING);
   ?>
<!-- Konten Utama -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
              Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Sekolah</a></li>
               <li class="breadcrumb-item active">Siswa</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-navy">
                  <h3 class="card-title">Master Data Siswa</h3>
               </div>
               <div class="card-body">
                  <table id="tabelsiswa" class="table table-bordered table-hover tabelsiswa bg-gradient-secondary">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>NIS</th>
                           <th>Nama</th>
                           <th>NIP</th>
                           <th>Nama Guru</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $no=1;
                           $data=mysqli_query($host, "SELECT * FROM siswa ORDER BY nis ASC") or die(mysqli_error($host));
                           while($d=mysqli_fetch_assoc($data)){
                           	$data2=mysqli_query($host,"SELECT * FROM guru WHERE nip='$d[nip]'");
                           	$d2=mysqli_fetch_assoc($data2);
                           ?>
                        <tr>
                           <td><?php echo $no++;?></td>
                           <td><?php echo $d['nis'];?></td>
                           <td><?php echo $d['nama'];?></td>
                           <td><?php echo $d['nip'];?></td>
                           <td><?php echo $d2['nama_guru']?></td>
                           <td class="text-center">
                              <a href="<?php if($ud[5] == 1){echo '../index/?page=edit_siswa&nis=';echo $d[nis];};?>" class="<?php if($ud[5] == 2){echo 'disabled';}; ?> btn btn-sm btn-primary">EDIT</a>
                              <button class="btn btn-sm btn-danger <?php if($ud[5] == 1){echo 'delete';}; ?>" <?php if($ud[5] == 2){echo 'disabled';}; ?>>Hapus</button>
                              <input type="hidden" class="the_nis" value='<?php echo $d['nis']; ?>'>
                           </td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="TambahData" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header bg-success">
            <h5 class="modal-title" id="TambahData">Tambah data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form method="post" action="../pages/siswa/tambah_siswa.php">
               <table class="table">
                  <tr>
                     <td>NIS</td>
                     <td><input type="text" name="nis" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Nama Siswa</td>
                     <td><input type="text" name="nama" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Guru</td>
                     <td>
                        <select name="nip" class="bg-gradient-light" style="width: 180px;">
                           <option value=0 selected>--Pilih guru--</option>
                           <?php
                              $data=mysqli_query($host,"SELECT * FROM guru ORDER BY nama_guru");
                              while($d=mysqli_fetch_assoc($data)){
                              	echo"
                              	<option value='$d[nip]'>$d[nama_guru]</option>";
                              }?>
                        </select>
                     </td>
                  </tr>
               </table>
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  <input class="btn btn-success tambah" type="submit" name="submit" value="Tambah" <?php if($ud[5] == 2){echo 'disabled';}; ?>>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script>
   $('.tabelsiswa').on("click", ".delete", function(e) {
      var id = $(this).closest('td').find('.the_nis').val();
      console.log(id);
      console.log('../index/?page=delete_siswa&nis=' + id);
      Swal.fire({
         title: "Apakah anda yakin?",
         text: "Ingin menghapus data Siswa dengan Nis " + id,
         icon: "warning",
         confirmButtonText: 'Hapus',
         cancelButtonText: 'Batal',
         showCancelButton: true,
         dangerMode: true,
      }).then((result) => {
         if(result.isConfirmed){
            window.location.href = "../pages/siswa/delete_siswa.php?nis=" + id;
         }else{
            Swal.fire("Data tidak jadi dihapus!");
         }
      });
   });
</script>