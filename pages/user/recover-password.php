<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Recover Password</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="../../plugins/dist/css/adminlte.min.css">
  <link rel="icon" href="../../icon.ico" type="image/ico" />
</head>
<body class="hold-transition login-page dark-mode">
<div class="login-box">

  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Harap hubungi <a href="mailto:zul.fahri.baihaqi19@mhs.ubharajaya.ac.id">admin</a> jika anda lupa password.</p>

      <form action="../../login.php" method="post">
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Kembali ke halaman login</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>


<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
</body>
</html>
