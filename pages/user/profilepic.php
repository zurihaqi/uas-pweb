<?php
   include "../user/session2.php";
   include "../../koneksi.php";
   error_reporting(E_ALL ^ E_WARNING);
    
      if(isset($_POST['submit'])){
        $username = mysqli_real_escape_string($host, $_SESSION['login']);
        $rand = rand();
        $ekstensi = array('png', 'jpg', 'jpeg', 'gif');
        $filename = $_FILES['foto']['name'];
        $ukuran = $_FILES['foto']['size'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        if (!in_array($ext, $ekstensi)) {
            $_SESSION["gagal"] = 'Ekstensi tidak diperbolehkan!';
        } else {
            if ($ukuran < 1044070) {
                $finalfoto = $rand . '_' . $filename;
                move_uploaded_file($_FILES['foto']['tmp_name'], 'assets/profilepic/' . $rand . '_' . $filename);
                mysqli_query($host, "UPDATE users SET foto = '$finalfoto' WHERE username = '$username'");
                $_SESSION["sukses"] = 'Foto berhasil diubah!';
            } else {
                $_SESSION["gagal"] = 'Ukuran file terlalu besar!';
            }
        }
      }
?>

<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Pengaturan Akun
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Pengaturan Akun</a></li>
               <li class="breadcrumb-item active">Foto Profil</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-3">
            <div class="card card-primary card-outline">
               <div class="card-body box-profile">
                  <div class="text-center">
                     <img class="profile-user-img img-circle"
                        src="../assets/profilepic/<?php printf($ud[4]);?>"
                        alt="User profile picture">
                  </div>
                  <h3 class="profile-username text-center"><?php printf($ud[1]);?></h3>
                  <p class="text-muted text-center">Akun dibuat pada tanggal:<br><span style="color: white;">
                     <?php 
                        printf($ud[3]);
                        ?></span>
                  </p>
               </div>
            </div>
            <div class="card card-primary">
            </div>
         </div>
            <div class="col-md-9">
                <div class="card">
                   <div class="card-header p-2">
                      <div class="card-title">Ubah Foto Profil</div>
                   </div>
                <div class="card-body">
                    <div class="tab-content">
                        <form class="form-horizontal" method="post" action="../index/?page=profilepic" enctype="multipart/form-data">
                            <div class="form-group row">
                               <label class="col-sm-2 col-form-label">Ubah Foto Profil</label>
                               <div class="custom-file col-sm-10">
                                  <input type="file" class="custom-file-input bg-light" name="foto" id="fotoId" <?php if($ud[5] == 2){echo 'disabled';}; ?>>
                                  <label class="custom-file-label bg-light" for="fotoId">Unggah Foto</label>
                               </div>
                                <div class="mt-3 col">
                                   <small class="text-red">(Jenis file yang diperbolehkan .png | .jpg | .jpeg | .gif)</small>
                                  <input type="submit" name="submit" class="btn btn-primary float-right" value="Unggah" <?php if($ud[5] == 2){echo 'disabled';}; ?>></input>
                               </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    $(function () {
      bsCustomFileInput.init();
    });
</script>