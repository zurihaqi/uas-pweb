$(function() {
    $("#tabelsiswa").DataTable({
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "scrollY": 200,
        "scrollX": true,
        "buttons": [
            {
                text: 'Tambah Data',
                className: 'btn-success',
                action: function () 
                {
                    $('#ModalTambah').modal('show');
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            }
        ]
    }).buttons().container().appendTo('#tabelsiswa_wrapper .col-md-6:eq(0)');
});
$(function() {
    $("#tabelguru").DataTable({
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "scrollY": 200,
        "scrollX": true,
        "buttons": [
            {
                text: 'Tambah Data',
                className: 'btn-success',
                action: function () 
                {
                    $('#ModalTambah').modal('show');
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            },
        ]
    }).buttons().container().appendTo('#tabelguru_wrapper .col-md-6:eq(0)');
});
$(function() {
    $("#tabelmapel").DataTable({
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "scrollY": 200,
        "scrollX": true,
        "buttons": [
            {
                text: 'Tambah Data',
                className: 'btn-success',
                action: function () 
                {
                    $('#ModalTambah').modal('show');
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [0, 1, 2]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [0, 1, 2]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [0, 1, 2]
                }
            },
        ]
    }).buttons().container().appendTo('#tabelmapel_wrapper .col-md-6:eq(0)');
});
$(function() {
    $("#tabelnilai").DataTable({
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "scrollY": 200,
        "scrollX": true,
        "buttons": [
            {
                text: 'Tambah Data',
                className: 'btn-success',
                action: function () 
                {
                    $('#ModalTambah').modal('show');
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                }
            },
        ]
    }).buttons().container().appendTo('#tabelnilai_wrapper .col-md-6:eq(0)');
});