<?php
   include "../user/session2.php";
   include "../../koneksi.php";
   if(isset($_GET['kd_mapel'])){
       $kd_mapel = $_GET['kd_mapel'];
       $select = mysqli_query($host, "SELECT * FROM mapel WHERE kd_mapel='$kd_mapel'") or die(mysqli_error($host));
       if(mysqli_num_rows($select) == 0){
           echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
           exit();
       }else{
           $d = mysqli_fetch_assoc($select);
       }
   }
   if(isset($_POST['submit'])){
       $nama_mapel  = $_POST['nama_mapel'];
   
       $sql = mysqli_query($host, "UPDATE mapel SET nama_mapel='$nama_mapel' WHERE kd_mapel='$kd_mapel'") or die(mysqli_error($host));
   
       if($sql){
           $_SESSION["sukses"] = 'Data mapel berhasil diedit!';
       }else{
           $_SESSION["gagal"] = 'Data mapel gagal diedit!';
           }
   }
   ?>
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Edit</a></li>
               <li class="breadcrumb-item active">Mapel</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-primary">
                  <h3 class="card-title">Edit Data mapel</h3>
               </div>
               <div class="card-body">
                  <form method="post" action="../index/?page=edit_mapel&kd_mapel=<?php echo $kd_mapel;?>">
                     <table class="table">
                        <tr>
                           <td>Kode Mapel</td>
                           <td><input type="text" name="kd_mapel" value="<?php echo $d['kd_mapel'];?>" readonly disabled></td>
                        </tr>
                        <tr>
                           <td>Nama Mapel</td>
                           <td><input type="text" name="nama_mapel" value="<?php echo $d['nama_mapel'];?>" required></td>
                        </tr>
                     </table>
                     <a href="../index/?page=mapel" class="btn btn-secondary">Kembali</a>
                     <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>