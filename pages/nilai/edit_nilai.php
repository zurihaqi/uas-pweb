<?php
   include "../user/session2.php";
   include "../../koneksi.php";
   if(isset($_GET['id_nilai'])){
       $id_nilai = $_GET['id_nilai'];
       $select = mysqli_query($host, "SELECT * FROM nilai WHERE id_nilai='$id_nilai'") or die(mysqli_error($host));
       if(mysqli_num_rows($select) == 0){
           echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
           exit();
       }else{
           $e = mysqli_fetch_assoc($select);
       }
   }
   if(isset($_POST['submit'])){
       $nis        = $_POST['nis'];
       $nip        = $_POST['nip'];
       $kd_mapel   = $_POST['kd_mapel'];
       $uts        = $_POST['uts'];
       $uas        = $_POST['uas'];
       $tugas      = $_POST['tugas'];
   
       $sql = mysqli_query($host, "UPDATE nilai SET nis='$nis', nip='$nip', kd_mapel='$kd_mapel', uts='$uts', uas='$uas', tugas='$tugas' WHERE id_nilai='$id_nilai'") or die(mysqli_error($host));
   
       if($sql){
           $_SESSION["sukses"] = 'Data nilai berhasil diedit!';
       }else{
           $_SESSION["gagal"] = 'Data nilai gagal diedit!';
           }
   }
   ?>
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Edit</a></li>
               <li class="breadcrumb-item active">Nilai</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-primary">
                  <h3 class="card-title">Edit Data Nilai</h3>
               </div>
               <div class="card-body">
                  <form method="post" action="../index/?page=edit_nilai&id_nilai=<?php echo $id_nilai;?>">
                     <table class="table">
                        <tr>
                           <td>ID Nilai</td>
                           <td><input type="text" name="id_nilai" value="<?php echo $e['id_nilai'];?>" readonly disabled></td>
                        </tr>
                        <tr>
                           <td>Siswa</td>
                           <td>
                              <select name="nis" class="bg-light" style="width: 180px;">
                                 <option value=0 selected>--Pilih siswa--</option>
                                 <?php
                                    $data=mysqli_query($host,"SELECT * FROM siswa ORDER BY nama");
                                    while($d=mysqli_fetch_assoc($data)){
                                    echo"
                                        <option value='$d[nis]'>$d[nama]</option>";
                                    }?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td>Guru</td>
                           <td>
                              <select name="nip" class="bg-light" style="width: 180px;">
                                 <option value=0 selected>--Pilih guru--</option>
                                 <?php
                                    $data=mysqli_query($host,"SELECT * FROM guru ORDER BY nama_guru");
                                    while($d=mysqli_fetch_assoc($data)){
                                    echo"
                                        <option value='$d[nip]'>$d[nama_guru]</option>";
                                    }?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td>Kode Mapel</td>
                           <td>
                              <select name="kd_mapel" class="bg-light" style="width: 180px;">
                                 <option value=0 selected>--Pilih mapel--</option>
                                 <?php
                                    $data=mysqli_query($host,"SELECT * FROM mapel ORDER BY nama_mapel");
                                    while($d=mysqli_fetch_assoc($data)){
                                    echo"
                                        <option value='$d[kd_mapel]'>$d[nama_mapel]</option>";
                                    }?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td>Nilai UTS</td>
                           <td><input type="text" name="uts" value="<?php echo $e['uts'];?>" required></td>
                        </tr>
                        <tr>
                           <td>Nilai UAS</td>
                           <td><input type="text" name="uas" value="<?php echo $e['uas'];?>" required></td>
                        </tr>
                        <tr>
                           <td>Nilai Tugas</td>
                           <td><input type="text" name="tugas" value="<?php echo $e['tugas'];?>" required></td>
                        </tr>
                     </table>
                     <a href="../index/?page=nilai" class="btn btn-secondary">Kembali</a>
                     <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>