<?php 
   include "../user/session2.php";
   include "../../koneksi.php";
   error_reporting(E_ALL ^ E_WARNING);
   
   if(isset($_POST['submit'])){
      $password1 = mysqli_real_escape_string($host, $_POST['newPassword']);
      $newname = mysqli_real_escape_string($host, $_POST['username']);
      $username = mysqli_real_escape_string($host, $_SESSION['login']);
      $password1=md5($password1);
         $sql = mysqli_query($host, "UPDATE users SET password='$password1', username='$newname' WHERE username='$username'") or die(mysqli_error($host));
      
         if($sql){
            echo "<script>alert('Ubah data berhasil! Silahkan melakukan login kembali.')</script>";
            session_destroy();
            echo "<script>url:location='../login/'</script>";
         }else{
            echo "<script>alert('Ubah data gagal.')</script>";
            }
      }
   ?>
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Pengaturan Akun
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Pengaturan Akun</a></li>
               <li class="breadcrumb-item active">Data Akun</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
               <div class="card-body box-profile">
                  <div class="text-center">
                     <img class="profile-user-img img-circle"
                        src="../assets/profilepic/<?php printf($ud[4]);?>"
                        alt="User profile picture">
                  </div>
                  <h3 class="profile-username text-center"><?php printf($ud[1]);?></h3>
                  <p class="text-muted text-center">Akun dibuat pada tanggal:<br>
                    <span style="color: white;">
                     <?php printf($ud[3]);?>
                    </span>
                  </p>
               </div>
            </div>
            <div class="card card-primary">
            </div>
         </div>
         <div class="col-md-9">
            <div class="card">
               <div class="card-header p-2">
                  <div class="card-title">Ubah Data Akun</div>
               </div>
               <div class="card-body">
                  <div class="tab-content">
                     <form class="form-horizontal" method="post" action="" name="usersetting">
                        <div class="form-group row">
                           <label class="col-sm-2 col-form-label">Ubah Username</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control bg-light" name="username" placeholder="Username" value="<?php printf($ud[1]);?>" required <?php if($ud[5] == 2){echo 'disabled';}; ?>>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-sm-2 col-form-label">Ubah Password</label>
                           <div class="col-sm-10">
                              <input type="password" class="form-control bg-light" name="newPassword" id="pass1" onChange="checkPasswordMatch();" placeholder="Password" required <?php if($ud[5] == 2){echo 'disabled';}; ?>>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-sm-2 col-form-label">Konfirmasi Password</label>
                           <div class="col-sm-10">
                              <input type="password" class="form-control bg-light" name="confirmPassword" id="pass2" onChange="checkPasswordMatch();" placeholder="Konfirmasi Password" required <?php if($ud[5] == 2){echo 'disabled';}; ?>>
                              <div class="checkbox">
                                 <label class="mt-3 col">
                                 <input type="checkbox" onclick="showPass()"> Tampilkan password</a>
                                 <input type="submit" onclick="return pass_validation()" name="submit" class="btn btn-danger float-right" value="Simpan" <?php if($ud[5] == 2){echo 'disabled';}; ?>></input>
                                 </label>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script src="../plugins/userconfig.js"></script>
