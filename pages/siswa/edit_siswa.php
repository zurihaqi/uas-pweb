<?php
   include "../user/session2.php";
   include "../../koneksi.php";
   if(isset($_GET['nis'])){
       $nis = $_GET['nis'];
       $select = mysqli_query($host, "SELECT * FROM siswa WHERE nis='$nis'") or die(mysqli_error($host));
       if(mysqli_num_rows($select) == 0){
           echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
           exit();
       }else{
           $d = mysqli_fetch_assoc($select);
       }
   }
   if(isset($_POST['submit'])){
       $nip    = $_POST['nip'];
       $nama   = $_POST['nama'];
   
       $sql = mysqli_query($host, "UPDATE siswa SET nama='$nama', nip='$nip' WHERE nis='$nis'") or die(mysqli_error($host));
   
       if($sql){
            $_SESSION['sukses'] = "Data siswa berhasil diedit!";
       }else{
            $_SESSION["gagal"] = 'Data siswa gagal diedit!';
           }
   }
   ?>
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
              Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Edit</a></li>
               <li class="breadcrumb-item active">Siswa</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-primary">
                  <h3 class="card-title">Edit Data Siswa</h3>
               </div>
               <div class="card-body">
                  <form method="post" action="../index/?page=edit_siswa&nis=<?php echo $nis;?>">
                     <table class="table">
                        <tr>
                           <td>NIS</td>
                           <td><input type="text" name="nis" value="<?php echo $d['nis'];?>" readonly disabled></td>
                        </tr>
                        <tr>
                           <td>Nama Siswa</td>
                           <td><input type="text" name="nama" value="<?php echo $d['nama'];?>" required></td>
                        </tr>
                        <tr>
                           <td>Guru</td>
                           <td>
                              <select name="nip" class="bg-gradient-light" style="width: 180px;">
                                 <option value=0 selected>--Pilih guru--</option>
                                 <?php
                                    $data=mysqli_query($host,"SELECT * FROM guru ORDER BY nama_guru");
                                    while($d=mysqli_fetch_assoc($data)){
                                    echo"
                                    <option value='$d[nip]'>$d[nama_guru]</option>";
                                    }?>
                              </select>
                           </td>
                        </tr>
                     </table>
                     <a href="../index/?page=siswa" class="btn btn-secondary">Kembali</a>
                     <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>