<?php 
   include "../user/session2.php";
   include "../../koneksi.php";
   error_reporting(E_ALL ^ E_WARNING);
   ?>
<!-- Konten Utama -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Sekolah</a></li>
               <li class="breadcrumb-item active">Nilai</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-navy">
                  <h3 class="card-title">Master Data Nilai</h3>
               </div>
               <div class="card-body">
                  <table id="tabelnilai" class="table table-bordered table-hover tabelnilai bg-gradient-secondary">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>ID Nilai</th>
                           <th>NIS</th>
                           <th>Nama Siswa</th>
                           <th>NIP</th>
                           <th>Nama Guru</th>
                           <th>Kode Mapel</th>
                           <th>Nama Mapel</th>
                           <th>Nilai UTS</th>
                           <th>Nilai UAS</th>
                           <th>Nilai Tugas</th>
                           <th>Predikat</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $no=1;
                           $data=mysqli_query($host, "SELECT * FROM nilai ORDER BY id_nilai ASC") or die(mysqli_error($host));
                           while($d=mysqli_fetch_assoc($data)){
                               $data2=mysqli_query($host,"SELECT * FROM guru WHERE nip='$d[nip]'");
                               $d2=mysqli_fetch_assoc($data2);
                               $data3=mysqli_query($host,"SELECT * FROM siswa WHERE nis='$d[nis]'");
                               $d3=mysqli_fetch_assoc($data3);
                               $data4=mysqli_query($host,"SELECT * FROM mapel WHERE kd_mapel='$d[kd_mapel]'");
                               $d4=mysqli_fetch_assoc($data4);
                               $hasil = (($d['tugas']*0.3) + ($d['uts']*0.3) + ($d['uas']*0.4));
                           ?>
                        <tr>
                           <td><?php echo $no++;?></td>
                           <td><?php echo $d['id_nilai'];?></td>
                           <td><?php echo $d['nis'];?></td>
                           <td><?php echo $d3['nama']?></td>
                           <td><?php echo $d['nip'];?></td>
                           <td><?php echo $d2['nama_guru']?></td>
                           <td><?php echo $d['kd_mapel']?></td>
                           <td><?php echo $d4['nama_mapel']?></td>
                           <td><?php echo $d['uts']?></td>
                           <td><?php echo $d['uas']?></td>
                           <td><?php echo $d['tugas']?></td>
                           <?php
                              if($hasil >= 80){
                                  echo "<td style='background-color:green; color:#fff;'>Baik</td>";
                              }else if($hasil >= 60){
                                  echo "<td style='background-color:blue; color:#fff;'>Cukup</td>";
                              }else{
                                  echo "<td style='background-color:red; color:#fff;'>Buruk</td>";
                              }
                              ?>
                           <td class="text-center">
                              <a href="<?php if($ud[5] == 1){echo '../index/?page=edit_nilai&id_nilai=';echo $d[id_nilai];};?>" class="btn btn-sm btn-primary <?php if($ud[5] == 2){echo 'disabled';}; ?>">EDIT</a>
                              <button class="btn btn-sm btn-danger <?php if($ud[5] == 1){echo 'delete';}; ?>" <?php if($ud[5] == 2){echo 'disabled';}; ?>>Hapus</button>
                              <input type="hidden" class="the_nilai" value='<?php echo $d['id_nilai']; ?>'>
                           </td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="TambahData" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header bg-success">
            <h5 class="modal-title" id="TambahData">Tambah data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form method="post" action="pages/nilai/tambah_nilai.php">
               <table class="table">
                  <tr>
                     <td>ID Nilai</td>
                     <td><input type="text" name="id_nilai" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Siswa</td>
                     <td>
                        <select name="nis" class="bg-light" style="width: 180px;">
                           <option value=0 selected>--Pilih siswa--</option>
                           <?php
                              $data=mysqli_query($host,"SELECT * FROM siswa ORDER BY nama");
                              while($d=mysqli_fetch_assoc($data)){
                                echo"
                                  <option value='$d[nis]'>$d[nama]</option>";
                              }?>
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Guru</td>
                     <td>
                        <select name="nip" class="bg-light" style="width: 180px;">
                           <option value=0 selected>--Pilih guru--</option>
                           <?php
                              $data=mysqli_query($host,"SELECT * FROM guru ORDER BY nama_guru");
                              while($d=mysqli_fetch_assoc($data)){
                                echo"
                                  <option value='$d[nip]'>$d[nama_guru]</option>";
                              }?>
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Kode Mapel</td>
                     <td>
                        <select name="kd_mapel" class="bg-light" style="width: 180px;">
                           <option value=0 selected>--Pilih mapel--</option>
                           <?php
                              $data=mysqli_query($host,"SELECT * FROM mapel ORDER BY nama_mapel");
                              while($d=mysqli_fetch_assoc($data)){
                                echo"
                                  <option value='$d[kd_mapel]'>$d[nama_mapel]</option>";
                              }?>
                        </select>
                     </td>
                  </tr>
                  <tr>
                     <td>Nilai UTS</td>
                     <td><input type="text" name="uts" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Nilai UAS</td>
                     <td><input type="text" name="uas" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Nilai Tugas</td>
                     <td><input type="text" name="tugas" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
               </table>
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  <input class="btn btn-success" type="submit" name="submit" value="Tambah" <?php if($ud[5] == 2){echo 'disabled';}; ?>>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script>
   $('.tabelnilai').on("click", ".delete", function(e) {
      var id = $(this).closest('td').find('.the_nilai').val();
      console.log(id);
      console.log('../index/?page=delete_nilai&id_nilai=' + id);
      Swal.fire({
         title: "Apakah anda yakin?",
         text: "Ingin menghapus data Nilai dengan ID Nilai " + id,
         icon: "warning",
         confirmButtonText: 'Hapus',
         cancelButtonText: 'Batal',
         showCancelButton: true,
         dangerMode: true,
      }).then((result) => {
         if(result.isConfirmed){
            window.location.href = "pages/nilai/delete_nilai.php?id_nilai=" + id;
         }else{
            Swal.fire("Data tidak jadi dihapus!");
         }
      });
   });
</script>