<?php 
   include "../user/session2.php";
   include "../../koneksi.php";
   error_reporting(E_ALL ^ E_WARNING);
   ?>
<!-- Konten Utama -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>
               Sekolah
            </h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a>Sekolah</a></li>
               <li class="breadcrumb-item active">Guru</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-navy">
                  <h3 class="card-title">Master Data Guru</h3>
               </div>
               <div class="card-body">
                  <table id="tabelguru" class="table table-bordered table-hover tabelguru bg-gradient-secondary">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>NIP</th>
                           <th>Nama</th>
                           <th>Jenis Kelamin</th>
                           <th>Nomor HP</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $no=1;
                           $data=mysqli_query($host, "SELECT * FROM guru") or die(mysqli_error($host));
                           while($d=mysqli_fetch_assoc($data)){
                           ?>
                        <tr>
                           <td><?php echo $no++; ?></td>
                           <td><?php echo $d['nip'];?></td>
                           <td><?php echo $d['nama_guru'];?></td>
                           <td><?php echo $d['jk'];?></td>
                           <td><?php echo $d['hp'];?></td>
                           <td class="text-center">
                              <a href="<?php if($ud[5] == 1){echo '../index/?page=edit_guru&nip=';echo $d[nip];};?>" class="btn btn-sm btn-primary <?php if($ud[5] == 2){echo 'disabled';}; ?>">EDIT</a>
                              <button class="btn btn-sm btn-danger <?php if($ud[5] == 1){echo 'delete';}; ?>" <?php if($ud[5] == 2){echo 'disabled';}; ?>>Hapus</button>
                              <input type="hidden" class="the_nip" value='<?php echo $d['nip']; ?>'>
                           </td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Modal Tambah -->
<div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="TambahData" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header bg-success">
            <h5 class="modal-title" id="TambahData">Tambah data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form method="post" action="pages/guru/tambah_guru.php">
               <table class="table">
                  <tr>
                     <td>NIP</td>
                     <td><input type="text" name="nip" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Nama Guru</td>
                     <td><input type="text" name="nama_guru" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
                  <tr>
                     <td>Jenis Kelamin</td>
                     <td>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input" type="radio" name="jk" value="L" required>
                           <label class="form-check-label">Laki-laki</label>
                        </div>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input" type="radio" name="jk" value="P" required>
                           <label class="form-check-label">Perempuan</label>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td>Nomor HP</td>
                     <td><input type="text" name="hp" required <?php if($ud[5] == 2){echo 'disabled';}; ?>></td>
                  </tr>
               </table>
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  <input class="btn btn-success" type="submit" name="submit" value="Tambah" <?php if($ud[5] == 2){echo 'disabled';}; ?>>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script>
   $('.tabelguru').on("click", ".delete", function(e) {
      var id = $(this).closest('td').find('.the_nip').val();
      console.log(id);
      console.log('../index/?page=delete_guru&nip=' + id);
      Swal.fire({
         title: "Apakah anda yakin?",
         text: "Ingin menghapus data Guru dengan Nip " + id,
         icon: "warning",
         confirmButtonText: 'Hapus',
         cancelButtonText: 'Batal',
         showCancelButton: true,
         dangerMode: true,
      }).then((result) => {
         if(result.isConfirmed){
            window.location.href = "pages/guru/delete_guru.php?nip=" + id;
         }else{
            Swal.fire("Data tidak jadi dihapus!");
         }
      });
   });
</script>