<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Daftar</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="../../plugins/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../../plugins/bootstrap/css/bootstrap.min.css">
  <link rel="icon" href="../../icon.ico" type="image/ico" />
</head>
<body class="hold-transition register-page">
  <img src="../../plugins/dist/img/school.png" width="100" height="80">
  <h2><span style="color:red">CRUD</span> Sekolah</h2>
<div class="register-box">

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Daftar keanggotaan baru <br> (hanya untuk tahap percobaan).</p>

      <form action="register_input.php" method="post" name="register">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input id="pass1" type="password" class="form-control" placeholder="Password" name="password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input id="pass2" type="password" class="form-control" placeholder="Konfirmasi Password" name="confirmPassword" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="" onclick="showPass()" id="flexCheckDefault">
          <label class="form-check-label" for="flexCheckDefault">
            Tampilkan Password
          </label>
        </div>
        <div class="row">
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" onclick="return pass_validation()">Daftar</button>
          </div>
        </div>
      </form>
      <a href="login.php" class="text-center">Saya sudah memiliki akun</a>
    </div>
        <?php 
          if(isset($_REQUEST["success"]))
          {
            echo "<script>
            alert('Akun berhasil dibuat!');
            window.location.href='login.php';
            </script>";
          }else if(isset($_REQUEST["dupe"]))
          {
            echo "<script>
            alert('Username sudah ada!');
            </script>";
          }
        ?>
  </div>
</div>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/dist/js/adminlte.min.js"></script>
<script>
  function showPass() {
    var x = document.getElementById("pass1");
    var y = document.getElementById("pass2");
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }if (y.type === "password") {
         y.type = "text";
      } else {
         y.type = "password";
      }
    }
</script>
<script>
function pass_validation()
   {
   var firstpassword=document.register.pass1.value;  
   var secondpassword=document.register.pass2.value;  

   if(firstpassword==secondpassword){
   return true;
   }  
   else{  
   alert("Password tidak sama.");  
   return false;  
   }  
   } 
</script>
<script>
  function success() {
    alert("Akun berhasil dibuat.");
  }
</script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
      var elements = document.getElementsByTagName("INPUT");
      for (var i = 0; i < elements.length; i++) {
          elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                  e.target.setCustomValidity("Kolom ini tidak boleh kosong.");
                }
          };
          elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
          };
      }
    })
</script>
</body>
</html>
