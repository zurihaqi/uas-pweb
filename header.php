<?php
   if(!isset($_SESSION["login"])){
    echo"
    <link rel='icon' href='icon.ico' type='../image/ico' />
    <script>alert('Anda belum melakukan login!')
    window.location.href = '../login/';</script>";
};
?>
<ul class="navbar-nav">
   <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
   </li>
   <li class="nav-item text-light mt-1">
   <a id="time"></a>
   </li>
</ul>
<ul class="navbar-nav ml-auto">
   <li class="nav-item dropdown user-menu">
      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
      <img src="../assets/profilepic/<?php printf($ud[4]);?>" class="user-image img-circle elevation-2" alt="User Image">
      <span class="d-none d-md-inline"><?php printf($ud[1]);?></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
         <li class="user-header bg-dark">
            <img src="../assets/profilepic/<?php printf($ud[4]);?>" class="img-circle elevation-2" alt="User Image">
            <p>
               Anda masuk sebagai<br><span style="color: yellow;"><?php printf($ud[1]);?></span>
            </p>
         </li>
         <li class="user-footer">
            <a href="../index/?page=setting" class="btn btn-secondary btn-flat float-left">Pengaturan</a>
            <a href="../pages/user/logout.php" class="btn btn-secondary btn-flat float-right">Keluar</a>
         </li>
      </ul>
   </li>
</ul>