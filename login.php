<?php
    if (isset($_POST['g-recaptcha-response'])) {
        $secret_key = '6LcnaIwbAAAAAOPhPliYC375OJeNAGSiRyhpakP4';
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response'];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        curl_close($curl);
        $responseCaptchaData = json_decode($data);
     
        if($responseCaptchaData->success) {
            include("koneksi.php");
            $a = $_REQUEST['username'];
            $b = $_REQUEST['password'];
            $b = md5($b);
            
            $myusername = mysqli_real_escape_string($host,$_POST['username']);
            $res = mysqli_query($host,"SELECT * FROM users where username='$a'and password='$b'");
            $result=mysqli_fetch_array($res);

            if($result)
                {   
                    session_start ();
                    $_SESSION["login"]="$myusername";
                    echo "
                    <script>location.href = '../index/?page=home'</script>";
                }else	
                {
                    header("location:../login/?err");
                }
            }else
            {
                header("location:../login/?errc");
        }
    }
?>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>

  <link rel="stylesheet" type="text/css" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" type="text/css" href="../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../plugins/dist/css/adminlte.min.css">
  <link rel="stylesheet" type="text/css" href="../plugins/bootstrap/css/bootstrap.min.css">
  <link rel="icon" href="icon.ico" type="../image/ico" />
</head>
<body class="hold-transition login-page dark-mode">
  <img src="../plugins/dist/img/school.png" width="100" height="80">
  <h2><span style="color:red">CRUD</span> Sekolah</h2>
<div class="login-box">
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Masuk untuk memulai sesi</p>

      <form action="../login/" method="post" id="loginform">
        <div class="input-group mb-3">
          <input type="username" name="username" class="form-control bg-light" placeholder="Username" required>
          <div class="input-group-append">
            <div class="input-group-text bg-light">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control bg-light" placeholder="Password" id="pass" required>
          <div class="input-group-append">
            <div class="input-group-text bg-light">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
         <div class="form-check col ml-3">
            <input class="form-check-input" type="checkbox" value="" onclick="showPass()" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
               Tampilkan Password
            </label>
          </div>
          <div class="col-4">
            <button type="submit" class="g-recaptcha btn btn-primary btn-block" data-sitekey="6LcnaIwbAAAAANyTVTrOxiEm_osPQbRkBdPMwqj6" data-callback="submitForm" name="sub">Masuk</button>
          </div>
          <div class="col">
            <label for="subguest" class="btn btn-secondary btn-block font-weight-normal">Demo</label>
          </div>
        </div>
      </form>
     <form action="../pages/user/guest.php" method="post" id="guestForm" class="float-right" hidden>
       <input type="submit" class="g-recaptcha" data-callback="guest" data-sitekey="6LcnaIwbAAAAANyTVTrOxiEm_osPQbRkBdPMwqj6" id="subguest" hidden></input>
     </form>
        <?php
        if (isset($_REQUEST["err"])) {
            echo"
            <p class='text-red text-center'>Username atau password salah.</p>
            ";
        }
        if (isset($_REQUEST["errc"])) {
            echo"
            <p class='text-red text-center'>Verifikasi captcha gagal.</p>
            ";
        }
        ?>
    </div>
  </div>
</div>
<footer class="mt-3">
    <strong><a>Zul Fahri Baihaqi</a></strong>
</footer>

  <script src="../plugins/jquery/jquery-3.6.0.min.js"></script>
  <script src="../plugins/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../plugins/dist/js/adminlte.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <script>
    function submitForm() {
        document.getElementById('loginform').submit();
    }
    function guest() {
        document.getElementById('guestForm').submit();
    }
  </script>
  <script>
    function showPass() {
    var x = document.getElementById("pass");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
    }
  </script>
</body>
</html>
